color codeschool
let g:NERDTreeWinPos = "right"
set guioptions-=T " Hide toolbar on launch
set guioptions-=r
set go-=L
set guifont=Monaco:h13 " Set default font
set guioptions=aAce
